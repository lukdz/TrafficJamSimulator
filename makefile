all: micro mini

micro: micro.c 
	gcc -o micro micro.c -I. -lm

mini: mini.cpp 
	g++ mini.cpp -o mini
